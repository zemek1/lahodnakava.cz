<?
/**
 * Zobrazeni titulni strany webu
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0.lahodnakava.cz
 */

//require_once ('./include/TText.php');
require_once ('include/TNews.php');
require_once ('include/TXml.php');
//$index=new TText();
$news=new TNews();
$xml=new TXml();

//strankovani
$stranka=10;
if(!isset($_GET[od]) || $_GET[od]==''){
	$_GET[od]=0;
	$od=0;
}
if(is_numeric($_GET[od])){
	$od = $_GET[od];
}
	$zapisu=$xml->getCountProducts();
	//generovani odkazu na podstranky
	$stranek=floor($zapisu/$stranka);
	$down = $od-$stranka;
	$up = $od+$stranka;
	if($up>=$zapisu){
		$up=($stranek*$stranka)-1;
	}
	if($down<0){
		$down=0;
	}
	$odkazy='<div class="odkazy"><a href="'.$xml->getConfigWeb('homeurl').'"> Začátek</a> | ';
	$odkazy.='<a href="'.$xml->getConfigWeb('homeurl').'/produkty/'.$down.'.html">Předchozí</a> | ';
	$odkazy.='<a href="'.$xml->getConfigWeb('homeurl').'/produkty/'.$up.'.html">Následující</a> | ';
	$konec = ($stranek*$stranka)-1;
	$odkazy.='<a href="'.$xml->getConfigWeb('homeurl').'/produkty/'.$konec.'.html">Konec </a></div>';

if(isset($_GET[getProduct])){
	$produkt=$xml->getProduct($_GET[getProduct]);
	$xml->assign($produkt, item);
	$xml->assign($produkt[0][product].' - '.$xml->getConfigWeb('title'), title);
    $xml->assign($produkt[0][product].' - '.$produkt[0][description], description);
} elseif(isset($_GET[category])) {
	$xml->assign($xml->getProducts($od,10,$_GET[category]), items);
    $xml->getCategoryName($_GET[categoryId]);
    $xml->assign($xml->getCategoryName($_GET[categoryId]).' - '.$xml->getConfigWeb('title'), title);
    $xml->assign($xml->getConfigWeb('description'), description);
} elseif ($_GET[action]=='find'){
	$xml->assign($xml->findProducts($_POST[keyword]), items);
	$xml->assign('Výsledky hledání - '.$xml->getConfigWeb('title'), title);
        $xml->assign($xml->getConfigWeb('description'), description);
} else {
	$xml->assign($xml->getProducts($od,10), items);
	$xml->assign($odkazy, strankovani);
	$xml->assign($xml->getConfigWeb('title'), title);
        $xml->assign($xml->getConfigWeb('description'), description);
}
	
$xml->assign($xml->getCategories(), category);
$xml->assign($news->getItems(), news);
?>