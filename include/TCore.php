<?php
/*
 *
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

class TCore{ 
	const FATAL_ERROR = true;
	const WARNING = true;
	
	function __construct(){
		session_start();
		error_reporting(0);
		$this->connect($this->getConfigDb());
		$this->template=$this->getTemplate();
		$this->setTemplate('templates/index.tpl.html');
		$this->assign($this->getConfigWeb('homeurl'), 'homeurl');
		$this->assign($this->getMenu(), 'menu');
		$this->assign($this->getConfigWeb('title'), title);
	}
	
	
	function __destruct(){
//                mysql_close();
		$this->template->parse();
		$this->template->output();
	}

	/**
	 * Vypise $co v sablone na $kam
	 * @param $co
	 * @param $kam
	 * @return unknown_type
	 */ 	
	function assign($co, $kam){
		$this->template->assign($co, $kam);
	}
	
	/**
	 * Nastavi sablonu pro vykresleni
	 * @param $path
	 * @return unknown_type
	 */
	function setTemplate($path){
		$this->template->readFile($path);
	}
	
//@FIXME otestovat funkcnost 
	function isComplete($povinne) {
	    $nevyplnene = array();
	    foreach ($povinne as $key => $val) {
	        if (!$_POST[$key]) {
	            $nevyplnene[] = $val;
	        }
	    }
	    return $nevyplnene;
	}
	
	/** Kontrola e-mailové adresy
	* @param string $email e-mailová adresa
	* @return bool syntaktická správnost adresy
	* @copyright Jakub Vrána, http://php.vrana.cz
	*/
	function validEmail($email) {
	    $atom = '[-a-z0-9!#$%&\'*+/=?^_`{|}~]'; // znaky tvořící uživatelské jméno
	    $domain = '[a-z0-9]([-a-z0-9]{0,61}[a-z0-9])'; // jedna komponenta domény
	    return eregi("^$atom+(\\.$atom+)*@($domain?\\.)+$domain\$", $email);
	}
	
	/**
	 * Pripojeni k DB
	 * @param $db
	 * @return unknown_type
	 */
	function connect($db){
		$spojeni=mysql_connect($db[server], $db[user], $db[password]);
		$databaze=mysql_select_db($db[db], $spojeni); 
		$kodovani = mysql_query("SET NAMES 'utf8';");
	}
	
	/**
	 * Vraci pole $db z config.php s nastavenim pro DB
	 * @return unknown_type
	 */
	function getConfigDb(){
		include 'config.php';
		return $db;
		
	}

	/**
	 * Vraci dle $key pozadovane nastaveni v $web v config.php nebo cele toto pole 
	 * @param $key
	 * @return unknown_type
	 */
	function getConfigWeb($key=NULL){
		include 'config.php';
		if($key!=NULL){
			return $web[$key];	
		} else {
			return $web;
		}
	}
	
	function getConfigFtp(){
		include 'config.php';
		return $ftpAccess;
		
	}
	
	/*
	 * 
	 */
	function getTemplate(){
		require_once 'class_pet.ger.inc.php';
		$template=new pet();
		return $template;
	}
	
	function getMenu(){
		$dotaz=mysql_query("SELECT * FROM menu ORDER by id");
		while($polozka=mysql_fetch_array($dotaz)){
			$menu.='<li><a href="'.$this->getConfigWeb('homeurl').'/'.$polozka[link].'"><img width="114px" height="24px" src="'.$this->getConfigWeb('homeurl').'/templates/images/'.$polozka[img].'" alt="'.$polozka[text].'" /></a></li>';
		}
		return $menu;
	}
	
	function getError($code){
		switch ($code) {
			case 'badlogin':
				return 'Zadané přihlašovací údaje nejsou platné.';
				break;
				
			case 'notlogin':
				return 'Pro tuto akci nemáte oprávnění.';
				break;
				
			case 'nevyplneno':
				return 'Nebyly vyplněny všechny požadované údaje.';
				break;
			
			default:
				return 'Chybová hláška nenalezena :-(';
				break;
		}
		
	}
	
	/**
	 * Odstrani diakritiku z retezce
	 * 
	 * @param $vstup
	 * @return unknown_type
	 */
	function diakritika($vstup){
		$a = array("á","ä","č","ď","é","ě","ë","í","ň","ó","ö","ř","š","ť","ú","ů","ü","ý","ž","Á","Ä","Č","Ď","É","Ě","Ë","Í","Ň","Ó","Ö","Ř","Š","Ť","Ú","Ů","Ü","Ý","Ž");
		$b = array("a","a","c","d","e","e","e","i","n","o","o","r","s","t","u","u","u","y","z","A","A","C","D","E","E","E","I","N","O","O","R","S","T","U","U","U","Y","Z");
		$bez_diakritiky = str_replace($a, $b, $vstup); 
		return $bez_diakritiky;
	 } 
	   
	/**
	 * nahradi mezery podtrzitkem
	 * @param $vstup
	 * @return unknown_type
	 */
	function mezerypryc($vstup){
		$bezmezer=str_replace(' ', '_', $vstup);
	  	return $bezmezer;
	}
	
	function diakAndSpace($vstup){
		return $this->diakritika($this->mezerypryc($vstup));	
	}
}
?>