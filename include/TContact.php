<?php
/**
 * 
 *
 * @author Lukas Zemek, http://lukas-zemek.cz, zemek@lukas-zemek.cz
 * @copyright 2009 Lukas Zemek
 * @version 1.0 
 */


require_once 'TCore.php';
class TContact extends TCore{
	var $subject='Zpráva z webu';
	
	var $from='info@vladamikulec.cz';
	
	var $for='zemek@lukas-zemek.cz';
	
	function __construct(){
		parent::__construct();
	}
	
	function send($data){
		include("securimage/securimage.php");
 		$img = new Securimage();
  		$valid = $img->check($data['code']);

		if($this->validEmail($_POST[email]) && $valid == true){
			/* message */
			$message = nl2br($_POST[message]);
			$message .="<br /><br /> <strong>Kontakty:</strong><br />";
			$message .="jméno:".$_POST[name].'<br />';
			$message .="tel.:".$_POST[phone];
			$message .="<br />email: <a href=\"mailto:".$_POST[email]."\">".$_POST[email]."</a><br />";
			
			/* Pokud chcete poslat HTML email, odkomentujte následující řádek */
			$headers .= "From: ".$this->from."\n";
			$headers .= "Content-Type: text/html; charset=utf-8\n"; // Mime typ
			
			
			/* a teď to odešleme */
			if(mail($this->for, $this->subject, $message, $headers)){
				$this->template->assign('Zpráva byla úspěšně odeslána.', error);
			} else {
				$this->template->assign('Zpráva nebyla odeslána.', error);
			}
		} else {
			$this->template->assign('Nebyly vyplněny požadované údaje.', error);
		}
	}
	
	function getForm($action){
		$form='
		<script type="text/javascript">
			window.addEvent("domready", function() {
				var exValidatorA = new fValidator("exA");	}
			);
		</script>
		<form id="exA" action="'.$action.'" method="post">
			<p><label for="name">Jméno</label> *<br />
			<input class="fValidate[\'required\']" type="text" id="name" name="name" size="40" /></p>
			
			<p><label for="email">E-mail</label> *<br />
			<input class="fValidate[\'required\',\'email\']" type="text" id="email" name="email" size="40" /> </p>
			
			<p><label for="phone">Telefon</label><br />
			<input type="text" id="phone" name="phone" size="40" /></p>
			
			<p><textarea id="message" name="message" cols="55" rows="5">Prostor pro Vás ...</textarea></p>
			
			<p><label for="phone">Ochrana proti spamu *</label><br />
			<img alt="captcha" id="image" class="floatleft" src="'.$this->getConfigWeb('homeurl').'/include/securimage/securimage_show.php?sid='.md5(uniqid(time())).'" />&nbsp;
			<a href="#" onclick="document.getElementById(\'image\').src = \''.$this->getConfigWeb('homeurl').'/include/securimage/securimage_show.php?sid=\' + Math.random(); return false"><img alt="reload" src="'.$this->getConfigWeb('homeurl').'/include/securimage/images/refresh.gif" /></a><br /><br />
			
			<br />
			<input type="text" name="code" size="30" /> <small>Přepište kód z obrázku do pole</small></p>
			<p>[* povinné ] <input type="submit" name="submitter" class="odesli" value="odeslat" /></p>
		</form>';
		return $form;
	}

}
?>