<?php
/**
 * Trida pro overovani administratoru systemu
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

require_once 'TCore.php';
class TAdmin extends TCore{
	
	function __construct(){
		parent::__construct();
	}
	
	/*
	 * Vraci id admina pokud $nickName a $password jsou platne, jinak vraci false
	 */
	function isAdmin($nickName, $password){
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("SELECT id FROM admins WHERE nickName = '$nickName' AND hashPassword = SHA1(CONCAT('$password', salt))");
		if(mysql_num_rows($dotaz)==1){
			$data=mysql_fetch_assoc($dotaz);
			return $data[id];
		} else {
			return false;
		}
	}
	
	/*
	 * Vrati asociativni pole s udaji o adminovi
	 * 
	 */
	function getAdmin($id){
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("SELECT * FROM admins WHERE id='$id'");
		return mysql_fetch_assoc($dotaz);
	}
	
	function sendEmail(){
		$for='zemek@lukas-zemek.cz';
		$subject='Přihlášení do systému '.$this->getConfigWeb('title');
		/* message */
		$message .="Uživatel ".$_SESSION[admin][name]." se přihlásil do administrace webu ".$this->getConfigWeb('title')." z IP ".$_SERVER[REMOTE_ADDR];
			
		/* Pokud chcete poslat HTML email, odkomentujte následující řádek */
		$headers .= "From: info@lukas-zemek.cz \n";
		$headers .= "Content-Type: text/html; charset=utf-8\n"; // Mime typ
			
		/* a teď to odešleme */
		mail($for, $subject, $message, $headers);
	}
	
	/*
	 * Zaloguje admina do systemu
	 */
	function loginAdmin($nickName, $password){
		$this->connect($this->getConfigDb());
		if($id=$this->isAdmin($nickName, $password)){
			$dotaz=mysql_query("UPDATE admins SET
								lastLogin= NOW(),
								loginIP='$_SERVER[REMOTE_ADDR]'
								WHERE id='$id'");
			$_SESSION[admin]=$this->getAdmin($id);
			$this->sendEmail();
			return true;
		} else {
			return false;		 
		}
	}
	
	
	/*
	 * Odhlasi admina ze systemu
	 * 
	 */
	function logoutAdmin(){
		unset($_SESSION[admin]);
		header('Location: ../index.php');
	}
	
	/*
	 * Vytvori admina se zadanymi parametry
	 * 
	 * @param array $params obsahuje parametry vytvareneho admina
	 * @return boolean
	 */
	function createAdmin($params){
		$salt=rand(0,99999);
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("INSERT INTO admins 
							(nickName, name, email, hashPassword, salt, lastLogin, loginIP, accessRight)
							VALUES (
							'$params[nickName]', '$params[name]', '$params[email]', SHA1(CONCAT('$params[password]', '$salt')),
							'$salt', NOW(), '$_SERVER[REMOTE_ADDR]', '$params[accessRight]');");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	function editAdmin($params){
		if(isset($params[accessRight])){
			$accessRight=',	accesRight=\'$params[accessRight]\'';
		}
		$dotaz=mysql_query("UPDATE admins SET
								nickName='$params[nickName]',
								name='$params[name]',
								email='$params[email]'
								".$accessRight."
								WHERE id='$params[id]'");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	function deleteAdmin($id){
//		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("DELETE FROM admins WHERE id='$id'");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	/*
	 * Vrati formular pro login
	 */
	function getLoginForm($action){
		$form='<form action="'.$action.'" method="post">
				<fieldset>
				<legend>Přihlášení do systému</legend>
				<table>
				<tr><th><label for="nickName">Jméno</label></th><td><input type="text" size="20" id="nickName" name="nickName" /></td></tr>
				<tr><th><label for="password">Heslo</label></th><td><input type="password" size="21" id="password" name="password" /></td></tr>
				<tr><th></th><td><input type="submit" name="submit" value="Přihlásit" /></td></tr>
				</table>
				</fieldset>
			  	</form>';
		return $form;
	}
	
	/*
	 * Vrati formular pro vytvoreni/editaci admina
	 */
	function getEditForm($action,$values=NULL){
		$form='<form id="exA" class="fValidator-form"  method="post">
				<fieldset>
				<legend>Přidání administrátora systému</legend>
				<table>
					<tr><th><label for="exA_nickName">Nick *</label></th><td><input id="exA_nickName"  class="fValidate[\'required\']" type="text" name="nickName" value="'.$values[nickName].'" /></td></tr>
					<tr><th><label for="name">Jméno *</label></th><td><input type="text" class="fValidate[\'required\',\'alpha\']" id="name" name="name" value="'.$values[name].'" /></td></tr>
					<tr><th><label for="exA_email">Email *</label></th><td><input type="text" id="exA_email" name="email" value="'.$values[email].'" class="fValidate[\'required\',\'email\']" /></td></tr>
				';
		if($values!=NULL){
			$form.='<tr><th><label for="password">Heslo *</label></th><td><input type="text" id="password" name="password" /></td></tr>
					<tr><th><label for="accessRight">Oprávnění</label></th><td>
						<select name="accessRight">
							<option value="0">Super admin</option>
							<option value="1">admin</option>
							<option value="2">user</option>							
					</td></tr>';	
		} else {
			if($values[accessRight]==0){
				$form.='<tr><th><label for="accessRight">Oprávnění</label></th><td>
							<select name="accessRight">
								<option value="0">Super admin</option>
								<option value="1">admin</option>
								<option value="2">user</option>							
						</td></tr>';
			}
		}
		$form.='<tr><td><input type="submit" value="Přidej" /></td></tr>
				</table>
				</fieldset>
			  </form>';
		return $form;
	}
	
	function adminMenu($accessRight){
		$menu='<ul>';
		switch ($accessRight){
			case 0: $menu.='<li><a ></a></li>';
			case 1: $menu.='<li><a ></a></li>';
			case 2: $menu.='<li><a ></a></li>';
		}
		$menu='</ul>';
	}
}
?>