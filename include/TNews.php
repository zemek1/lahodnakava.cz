<?php
/**
 * 
 *
 * @author Lukas Zemek, http://lukas-zemek.cz, zemek@lukas-zemek.cz
 * @copyright 2009 Lukas Zemek
 * @version 1.0 
 */


require_once 'TCore.php';
class TNews extends TCore{
	
	function __construct(){
		session_start();
		$this->template=$this->getTemplate();
	}

	/**
	 * Vlozi novinku do DB
	 * @param $data
	 * @return unknown_type
	 */
	function insertRecord($data){
		$this->connect($this->getConfigDb());
		$adminId=$_SESSION[admin][id];
		$dotaz=mysql_query("INSERT INTO news 
							(adminId, prefixText, createDate)
							VALUES (
									'$adminId',
									'$data[prefixText]',
									NOW());
							");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Upravi novinku v DB 
	 * @param $data
	 * @return unknown_type
	 */
	function editRecord($data){
		$this->connect($this->getConfigDb());
		$adminId=$_SESSION[admin][id];
		$dotaz=mysql_query("UPDATE news SET
							  adminId = '$adminId', 
							  prefixText = '$data[prefixText]',
							  createDate = NOW()
							WHERE id = '$data[id]'");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Smaze novinku z DB
	 * @param $id
	 * @return unknown_type
	 */
	function deleteRecord($id){
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("DELETE FROM news WHERE id = '$id'");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	/**
	 * Vraci posledni novinky
	 * @param $number
	 * @param $lang
	 * @return unknown_type
	 */
	function getItems($number=5,$lang=cz,$full=false){
                if($full){
                    $dotaz=mysql_query("SELECT DATE_FORMAT(createDate, '%e.%c.%Y') as createDate, prefixText, fullText FROM news WHERE lang = '$lang' ORDER by id desc LIMIT ".$number.";");
                } else {
                    $dotaz=mysql_query("SELECT DATE_FORMAT(createDate, '%e.%c.%Y') as createDate, prefixText FROM news WHERE lang = '$lang' ORDER by id desc LIMIT ".$number.";");
                }
                echo mysql_error();
                while ($items[]=mysql_fetch_array($dotaz)){
		}
		unset($items[count($items)-1]);		//smazani prazdneho posledniho prvku
		return $items;
	}
	
	/**
	 * Form pro přidání novinky
	 * @param $action
	 * @return unknown_type
	 */
	function getInsertForm($action,$lang){
		$form='<div class="table">
				<img src="../templates/img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="../templates/img/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<form action="'.$action.'" method="post" enctype="multipart/form-data">
				<table class="listing form" cellpadding="0" cellspacing="0">
				<tr><th class="full" colspan="2">Vložení novinky '.$lang.'</th></tr>
				<tr><th><label for="prefixText">Úvodní text</label></th><td class="last"><textarea name="prefixText" id="prefixText" rows="5" cols="70"></textarea></td></tr>
                                <tr><th><label for="fullText">Hlavní text</label></th><td class="last"><textarea name="fullText" id="fullText" rows="15" cols="70"></textarea></td></tr>
				<input type="hidden" name="lang" value="'.$lang.'">
				<input type="hidden" name="fullText" value="NULL">
				<tr><th class="full" colspan="2"><input type="submit" name="submit" value="Přidat" /></th></tr>
				</table>
			  	</form><p>&nbsp;</p>
		  </div>';
		return $form;
	}
	
	function getEditTable($lang){
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("SELECT LEFT(prefixText, 100) as text, lang, id, createDate FROM news WHERE lang = '$lang' ORDER by id desc");
		$table='<div class="table">
				<img src="'.$this->getConfigWeb('homeurl').'/templates/img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="'.$this->getConfigWeb('homeurl').'/templates/img/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<table class="listing" cellpadding="0" cellspacing="0">
					<tr>
						<th class="first" width="300">Text</th>
						<th>Jazyk</th>
						<th>Vytvořeno</th>
						<th>Edit</th>
						<th class="last">Smaž</th>
					</tr>';
		while($row=mysql_fetch_array($dotaz)){
			$table.='<tr>
						<td class="first style1">'.htmlspecialchars($row[text]).'</td>
						<td>'.$row[lang].'</td>
						<td>'.$row[createDate].'</td>
						<td><a href="news.php?action=edit&id='.$row[id].'"><img src="'.$this->getConfigWeb('homeurl').'/templates/img/save-icon.gif" width="16" height="16" alt="save" /></td>
						<td class="last"><a href="news.php?action=delete&id='.$row[id].'"><img src="'.$this->getConfigWeb('homeurl').'/templates/img/hr.gif" width="16" height="16" alt="del" /></td>
					</tr>';
		}
		$table.='</table></div>';
		return $table;
	}
	
	function getEditForm($id,$action){
		$this->connect($this->getConfigDb());
		$dotaz=mysql_query("SELECT * FROM news WHERE id = '$id'");
		$data=mysql_fetch_array($dotaz);
		$form='<form action="'.$action.'" method="post">
				<fieldset>
				<legend>Editace novinky '.$lang.'</legend>
				<table>
				<tr><th><label for="prefixText">Úvodní text</label></th><td><textarea name="prefixText" id="prefixText" rows="5" cols="70">'.$data[prefixText].'</textarea></td></tr>
				<input type="hidden" name="lang" value="'.$lang.'">
				<input type="hidden" name="id" value="'.$id.'">
				<input type="hidden" name="fullText" value="NULL">
				<tr><th></th><td><input type="submit" name="submit" value="Upravit" /></td></tr>
				</table>
				</fieldset>
			  	</form>';
		return $form;
	}
}
?>