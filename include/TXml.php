<?php
/**
 *
 *
 * @author Lukas Zemek, http://lukas-zemek.cz, zemek@lukas-zemek.cz
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

require_once 'TCore.php';
// include Texy!
require_once 'texy/texy.php';
class TXml extends TCore {
    var $file='../include/xml/feed.xml';

    function __construct() {
        parent::__construct();
    }

    function downloadXml($url) {
        $file=file_get_contents($url);
        $lokalni = fopen('../include/xml/feed.xml', w);
        if(!$lokalni) {
            return  'Nepodarilo se otevrit lokalni listek';
        }
//        $test=iconv('ISO-8859-2', 'UTF-8//TRANSLIT', $file);

        fwrite($lokalni, $file);
        if(fclose($lokalni)) {
            return 'XML bylo staženo a uloženo';
        } else {
            return 'XML NEBYLO staženo a uloženo';
        }
    }

    function getCategories() {
        $dotaz=mysql_query("SELECT * FROM category");
        for($i=0;$i<mysql_num_rows($dotaz);$i++) {
            $items[$i]=mysql_fetch_array($dotaz);
            $items[$i][_category]=$this->diakAndSpace($items[$i][category]);
            $items[$i][homeurl]=$this->getConfigWeb('homeurl');
        }
        return $items;
    }

    function getCategoryName($id) {
        $dotaz=mysql_query("SELECT category FROM category WHERE id = '$id'");
        $data=mysql_fetch_array($dotaz);
        return $data[category];
    }

    function getProducents() {
        $dotaz=mysql_query("SELECT DISTINCT * FROM znacka");
        while ($items[]=mysql_fetch_array($dotaz)) {
        }
        unset($items[count($items)-1]);		//smazani prazdneho posledniho prvku
        return $items;
    }

    function getProducts($from,$to,$category=NULL) {
        $texy=new Texy();
        if($category!=NULL) {
            $dotaz=mysql_query("SELECT * FROM products WHERE categorytext like '%$category%' ");
        } else {
            $dotaz=mysql_query("SELECT * FROM products ORDER by categorytext LIMIT $from, $to");
        }
        for($i=0;$i<mysql_num_rows($dotaz);$i++) {
            $items[$i]=mysql_fetch_array($dotaz);
            $items[$i][description]=$texy->process($items[$i][description]);
            $items[$i][homeurl]=$this->getConfigWeb('homeurl');
            $items[$i][cenabezna]=number_format($items[$i][cenabezna],0, ',', ' ');
            $items[$i][cenapartner]=number_format($items[$i][cenapartner],0, ',', ' ');
            $items[$i][_product]=str_replace('&','&amp;',$this->diakAndSpace($items[$i][product]));
            $items[$i][product]=str_replace('&','&amp;',$items[$i][product]);
            $items[$i][_categorytext]=str_replace('&','&amp;',$this->diakAndSpace($items[$i][categorytext]));
            $items[$i][url]=str_replace('&','&amp;',$items[$i][url]);
            if(!file_exists('images/products/'.$items[$i][id].'.jpg')) {
                $items[$i][img]='images/products/thumb/noimg.jpg';
            } else {
                $items[$i][img]='images/products/thumb/'.$items[$i][id].'.jpg';
            }
        }
        return $items;
    }

    function getCountProducts() {
        $dotaz=mysql_query("SELECT COUNT(id) as count FROM products");
        $items=mysql_fetch_array($dotaz);
        return $items[count];
    }

    function getProduct($id) {
        $texy=new Texy();
        $dotaz=mysql_query("SELECT * FROM products WHERE id = $id");
        $items[]=mysql_fetch_array($dotaz);
        $items[0][longdescription]=$texy->process($items[0][longdescription]);
        $items[0][longdescription]=str_replace('.html', '.html?partner=1110', $items[0][longdescription]);
        $items[0][homeurl]=$this->getConfigWeb('homeurl');
        $items[0][cenabezna]=number_format($items[0][cenabezna],0, ',', ' ');
        $items[0][cenapartner]=number_format($items[0][cenapartner],0, ',', ' ');
        $items[0][_product]=$this->diakAndSpace($items[0][product]);
        return $items;
    }
    function display_xml_error($error, $xml) {
        $return  = $xml[$error->line - 1] . "\n";
        $return .= str_repeat('-', $error->column) . "^\n";

        switch ($error->level) {
            case LIBXML_ERR_WARNING:
                $return .= "Warning $error->code: ";
                break;
            case LIBXML_ERR_ERROR:
                $return .= "Error $error->code: ";
                break;
            case LIBXML_ERR_FATAL:
                $return .= "Fatal Error $error->code: ";
                break;
        }

        $return .= trim($error->message) .
                "\n  Line: $error->line" .
                "\n  Column: $error->column";

        if ($error->file) {
            $return .= "\n  File: $error->file";
        }

        return "$return\n\n--------------------------------------------\n\n";
    }
    function importToDB() {
        libxml_use_internal_errors(true);
        $dotaz=mysql_query("UPDATE products SET active = false;");
        echo $this->file;
        $xml=simplexml_load_file($this->file);

        if(!$xml) {
            $errors = libxml_get_errors();

            foreach ($errors as $error) {
                echo $this->display_xml_error($error, $xml);
            }

            libxml_clear_errors();
            
            return false;
        }
        foreach ($xml->SHOPITEM as $item) {
            if($item->CENA=='') {
                continue ;
            }
            if($item->CENABEZNA=='') {
                $item->CENABEZNA=$item->CENA;
            }
            if($item->CENAPARTNER=='') {
                $item->CENAPARTNER=$item->CENA;
            }
            $dotaz=mysql_query("UPDATE products SET
                                    znacka = '".$item->ZNACKA."',
                                    categorytext = '".$item->CATEGORYTEXT."',
                                    description = '".$item->DESCRIPTION."',
                                    longdescription = '".$item->LONGDESCRIPTION."',
                                    url = '".$item->URL."',
                                    imgurl = '".$item->IMGURL."',
                                    cena = '".$item->CENA."',
                                    cenabezna = '".$item->CENABEZNA."',
                                    cenapartner = '".$item->CENAPARTNER."',
                                    active = 1
 				WHERE url = '".$item->URL."';"
            );
            if(mysql_affected_rows()==2) {
                echo $item->PRODUCT.' '.$item->URL.'<br>';
            }
            if(mysql_affected_rows()==0) {
                echo('dsad');
                $dotaz=mysql_query("INSERT INTO products(
										product,
										znacka,
										categorytext,
										description,
										longdescription,
										url,
										imgurl,
										cena,
										cenabezna,
										cenapartner,
										active
										)
									VALUES(
										'$item->PRODUCT',
										'$item->ZNACKA',
										'$item->CATEGORYTEXT',
										'$item->DESCRIPTION',
										'$item->LONGDESCRIPTION',
										'$item->URL',
										'$item->IMGURL',
										'$item->CENA',
										'$item->CENABEZNA',
										'$item->CENAPARTNER',
										'1'
										)");

                echo mysql_error();
            }
        }
        echo mysql_error();
        if($dotaz) {
            return true;
        } else {
            return false;
        }
    }

    function downloadImages() {
        $dotaz=mysql_query("SELECT id,imgurl FROM products");
        while($data=mysql_fetch_array($dotaz)) {
            $hlavicka=get_headers($data[imgurl]);
            if(!strstr($hlavicka[0], 'HTTP/1.1 404 Not Found')) {
                $fpExt = FOpen($data[imgurl],'rb');
                if($fpExt) {
                    $contentExt = '';
                    //* stažení obrázku
                    while (!FEof($fpExt)) {
                        $contentExt .= FRead( $fpExt, 100000 );
                    }
                    FClose( $fpExt );

                    // uložení obrázku na disk
                    $localFilename='../images/products/'.$data[id].'.jpg';
                    $fpInt = FOpen($localFilename, 'w' );
                    if(!FWrite( $fpInt, $contentExt )) {
                        $err=1;
                    }
                    FClose( $fpInt );
                }
            }

        }
        if($err) {
            return false;
        } else {
            return true;
        }
    }

    function resizeImages() {
        exec('mogrify  -format jpg -path ../images/products/thumb -thumbnail 202 ../images/products/*.jpg',$x,$z);
        if($z==0) {
            return true;
        } else {
            return false;
        }
    }

    function findProducts($keyword) {
        $keyword=addslashes($keyword);
        $dotaz = mysql_query("
							    SELECT *
							    FROM products
							    WHERE MATCH (product) AGAINST ('$keyword')
							    OR MATCH (description) AGAINST ('$keyword')
							    OR MATCH (longdescription) AGAINST ('$keyword');
							    
                ");
        //OR MATCH(description) AGAINST ('$keyword' IN BOOLEAN MODE)
        //OR MATCH(longdescription) AGAINST ('$keyword' IN BOOLEAN MODE)
        //ORDER BY 5 * MATCH(product) AGAINST ('$keyword]') + MATCH(description) AGAINST ('$keyword') DESC
        $texy=new Texy();
        for($i=0;$i<mysql_num_rows($dotaz);$i++) {
            $items[$i]=mysql_fetch_array($dotaz);
            $items[$i][description]=$texy->process($items[0][description]);
            $items[$i][homeurl]=$this->getConfigWeb('homeurl');
            $items[$i][cenabezna]=number_format($items[$i][cenabezna],0, ',', ' ');
            $items[$i][cenapartner]=number_format($items[$i][cenapartner],0, ',', ' ');
            $items[$i][_product]=str_replace('&','&amp;',$this->diakAndSpace($items[$i][product]));
            $items[$i][product]=str_replace('&','&amp;',$items[$i][product]);
            $items[$i][url]=str_replace('&','&amp;',$items[$i][url]);
            if(!file_exists('images/products/'.$items[$i][id].'.jpg')) {
                $items[$i][img]='images/products/thumb/noimg.jpg';
            } else {
                $items[$i][img]='images/products/thumb/'.$items[$i][id].'.jpg';
            }
        }
        $nadpis='<h2>Výsledky hledání<h2>';
        return $items;
    }

    function generateXmlExport() {
        //ziskani produktu z db
        $items=$this->getProducts(0,500);

        //otevreni xml feedu
        $zbozi=fopen('../xml/zbozi.xml', 'w+');
        fwrite($zbozi,'<?xml version="1.0" encoding="utf-8" ?>
						<SHOP>');
        foreach ($items as $item) {
            fwrite($zbozi, '<SHOPITEM>
							<PRODUCT>'.$item[product].'</PRODUCT>
                            <DESCRIPTION>'.strip_tags($item[description]).'</DESCRIPTION>
                            <URL>'.$this->getConfigWeb('homeurl').'/produkt/'.$item[id].'/'.$item[_product].'.html</URL>
                            <ITEM_TYPE>new</ITEM_TYPE>
                            <AVAILABILITY>72</AVAILABILITY>
                            <IMGURL>'.$this->getConfigWeb('homeurl').'/images/products/'.$item[id].'.jpg</IMGURL>
							<PRICE_VAT>'.$item[cenapartner].'</PRICE_VAT>
						   </SHOPITEM>');
        }
        fwrite($zbozi,'</SHOP>');
        if(fclose($zbozi)) {
            return 'XML bylo vygenerovano';
        } else {
            return 'XML NEBYLO vygenerovano';
        }
    }
}
?>