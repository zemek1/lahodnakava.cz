<?php
/**
 * Trida pro 
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

require_once 'TCore.php';
class TText extends TCore{
	
	function __construct(){
		parent::__construct();
	}
	
	
	function insertText($data){
		$adminId=$_SESSION[admin][id];
		$dotaz=mysql_query("INSERT INTO text
							(adminId, title, text, createDate)
							VALUES (
								'$adminId', '$data[title]', '$data[text]', NOW()
							);");
		if($dotaz){
			return true;
		} else {
			return false;
		}
	}
	
	function editText($data){
		$adminId=$_SESSION[admin][id];
		$dotaz=mysql_query("UPDATE text SET
								adminId = '$adminId',
								title = '$data[title]',
								text = '$data[text]',
								createDate = NOW()
							WHERE id = '$data[id]'");
		if($dotaz){
			$this->assign('Úspěšně upraveno', error);
			return true;
		} else {
			$this->assign('Nepovedlo se upravit', error);
			return false;
		}
	}
	
	function deleteText($id){
		$dotaz=mysql_query("DELETE FROM text WHERE id = '$id'");
		if($dotaz){
			$this->assign('Úspěšně smazáno', error);
			return true;
		} else {
			$this->assign('Nepovedlo se smazat', error);
			return false;
		}
	}
	
	function getText($id=false){
		if(is_int($id)){
			$dotaz=mysql_query("SELECT * FROM text WHERE id = '$id'");
			$data=mysql_fetch_array($dotaz);
			return $data;
		}	
	}
	
	function getEditTable(){
		$dotaz=mysql_query("SELECT 
								LEFT(text, 100) as text,
								id, 
								title,
								createDate 
							FROM text 
							ORDER by id desc");
		$table='<div class="table">
				<img src="'.$this->getConfigWeb('homeurl').'/templates/img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="'.$this->getConfigWeb('homeurl').'/templates/img/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<table class="listing" cellpadding="0" cellspacing="0">
					<tr>
						<th class="first">Stránka</th>
						<th width="280">Text</th>
						<th>Vytvořeno</th>
						<th>Edit</th>';
						if($_SESSION[admin][accessRight]==1){
							$table.='<th class="last">Smaž</th>';
						}
					$table.='</tr>';
		while($row=mysql_fetch_array($dotaz)){
			//nevypise text s error hlaskou
			if($row[id]==0){
				continue;
			}
			$table.='<tr>
						<td class="first style1">'.$row[title].'</td>
						<td>'.htmlspecialchars($row[text]).'</td>
						<td>'.$row[createDate].'</td>
						<td><a href="text.php?action=getEditForm&id='.$row[id].'"><img src="'.$this->getConfigWeb('homeurl').'/templates/img/save-icon.gif" width="16" height="16" alt="save" /></td>';
						if($_SESSION[admin][accessRight]==1){
							$table.='<td class="last"><a href="text.php?action=delete&id='.$row[id].'"><img src="'.$this->getConfigWeb('homeurl').'/templates/img/hr.gif" width="16" height="16" alt="del" /></td>';
						}
					$table.='</tr>';
		}
		$table.='</table></div>';
		return $table;
	}
	
	function getInsertForm($action){
		$form='<div class="table">
				<img src="../templates/img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="../templates/img/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<form name="insertform" action="'.$action.'" method="post">
				<table class="listing form" cellpadding="0" cellspacing="0">
				<tr><th class="full" colspan="2">Přidání textové stránky</th></tr>
				<tr><th><label for="title">Titulek</label></th><td class="last"><input type="text" size="40" id="title" name="title" value="'.$data[category].'" /></td></tr>
				<tr><th><label for="text">Text</label></th><td class="last"><textarea name="text" id="text" rows="20" cols="80">'.$data[description].'</textarea></td></tr>
				<tr><th class="full" colspan="2"><input type="submit" name="submit" value="Upravit" /></th></tr>
				</table>
				<input type="hidden" name="id" value="'.$data[id].'" />
			  	</form><p>&nbsp;</p>
		  </div>';
		return $form;
	}
	
	function getEditForm($id,$action){
		$data=$this->getText((int)$id);
		$form='<div class="table">
				<img src="../templates/img/bg-th-left.gif" width="8" height="7" alt="" class="left" />
				<img src="../templates/img/bg-th-right.gif" width="7" height="7" alt="" class="right" />
				<form name="insertform" action="'.$action.'" method="post">
				<table class="listing form" cellpadding="0" cellspacing="0">
				<tr><th class="full" colspan="2">Přidání textové stránky</th></tr>
				<tr><th><label for="title">Titulek</label></th><td class="last"><input type="text" size="40" id="title" name="title" value="'.$data[title].'" /></td></tr>
				<tr><th><label for="text">Text</label></th><td class="last"><textarea name="text" id="text" rows="20" cols="80">'.$data[text].'</textarea></td></tr>
				<tr><th class="full" colspan="2"><input type="submit" name="submit" value="Upravit" /></th></tr>
				</table>
				<input type="hidden" name="id" value="'.$data[id].'" />
			  	</form><p>&nbsp;</p>
		  </div>';
		return $form;
	}
}
?>