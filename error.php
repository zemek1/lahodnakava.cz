<?
/**
 * 
 * 
 * @author Lukas Zemek
 * @copyright 2008 Lukas Zemek
 * @version 0.1 
 */

require_once ('include/TNews.php');
require_once ('include/TXml.php');

$news=new TNews();
$xml=new TXml();

header("HTTP/1.0 404 Not Found");
$obsah='<h2>Stránka nenalezena - 404 ERROR</h2>
		<p>Je nám líto, ale požadovaná stránka se na webu nenachází. Zkontrolujte prosím, zadaní URL.</p>
        <p>Dovolujeme si Vám proto nabídnout produkty z naší akční nabídky.</p>';

$xml->assign($obsah, error);
$xml->assign($xml->getProducts(0,3), items);
$xml->assign($xml->getCategories(), category);
$xml->assign($news->getItems(), news);
$xml->assign('ERORR 404 - '.$xml->getConfigWeb('title'), title);
?>