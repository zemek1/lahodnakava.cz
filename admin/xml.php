<?
/**
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 0.5
 */

require_once ('../include/TXml.php');
$xml=new TXml();
$xml->setTemplate('../templates/admin.tpl.html');
set_time_limit(100000);
//je to admin?
if(isset($_SESSION[admin])){
	switch ($_GET[action]){
        case 'downloadXml':	$vystup=$xml->downloadXml($xml->getConfigWeb('xml'));
				  			break;
		case 'downloadImg': if($xml->downloadImages()){
                                        if($xml->resizeImages()){
                                            $vystup='Stazeni a zmenseni obrazku OK';
                                        } else {
                                            $vystup='Obrazky NEBYLY zmenseny, stazeni OK';
                                        }
                                    } else {
                                        $vystup='Obrazky NEBYLY stazeny';
                                    }
                                    break;
		case 'importXml': 	if($xml->importToDB()){
                                            $vystup='Import dat OK';
                                        } else {
                                            $vystup='Import dat selhal';
                                        }
                                        break;
                case 'resizeImg': 	if($xml->resizeImages()){
                                            $vystup='Zmenseni OK';
                                        } else {
                                            $vystup='Zmenseni selhalo';
                                        }
                                        break;
		case 'exportXml': 	$vystup=$xml->generateXmlExport(0);
							break;


//		default: $vystup=$xml->importToDB();
//				  break;
	}
} else {
	header('Location: ../admin.php?err=notlogin');
}

$menu='
	<h3>xml</h3>
			<ul class="nav">';
if($_SESSION[admin][accessRight]==1){
//	$menu.='<li><a href="xml.php?action=getInsertForm">Přidej xml</a></li>';
	$menu.='<li><a href="xml.php?action=downloadXml">Stáhni xml</a></li>';
	$menu.='<li><a href="xml.php?action=importXml">Importuj xml</a></li>';
	$menu.='<li><a href="xml.php?action=downloadImg">Stáhni obrázky</a></li>';
        $menu.='<li><a href="xml.php?action=resizeImg">Zmensi obrázky</a></li>';
	$menu.='<li><a href="xml.php?action=exportXml">Generuj XML</a></li>';
}
$menu.='<li><a href="xml.php">Uprav xml</a></li>
	</ul>';

$xml->assign($vystup, vystup);
$xml->assign($table, table);
$xml->assign('xml', heading);
$xml->assign($menu, adminmenu);
$xml->assign('class="active"', xml);
$xml->assign($xml->getConfigWeb('title').' - Administrace', title);
?>