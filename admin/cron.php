<?
/**
 *
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

require_once ('../include/TXml.php');
$xml=new TXml();

$xml->downloadXml($xml->getConfigWeb('xml'));
$xml->importToDB();
$xml->downloadImages();
$xml->generateXmlExport();
?>