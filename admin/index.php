<?
/**
 * 
 * 
 * @author Lukas Zemek
 * @copyright 2008 Lukas Zemek
 * @version 1.0 
 */

require_once ('../include/TAdmin.php');
$admin=new TAdmin();
$template=$admin->getTemplate();
$template->readfile('../templates/admin.tpl.html');

//je to admin?
if(isset($_SESSION[admin])){
	$vystup='<strong>Dobrý den,</strong><br /><br />
			 Vaše přihlášení bylo úspěšné. Jste přihlášen z IP '.$_SESSION[admin][loginIP];
} else {
	header('Location: ../admin.php?err=notlogin');
}
$template->assign($vystup, vystup);
$template->assign($error, error);
$template->assign('Home page', heading);
$template->assign('class="active"', homepage);
$template->assign($admin->getConfigWeb('title').' - Administrace', title);
$template->parse();
$template->output();
?>