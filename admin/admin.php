<?
/**
 * 
 * @author Lukas Zemek
 * @copyright 2008 Lukas Zemek
 * @version 0.1 
 */

require_once ('../include/TAdmin.php');
$admin=new TAdmin();
$admin->setTemplate('../templates/admin.tpl.html');


//je to admin?
if(isset($_SESSION[admin])){
	switch ($_GET[action]){
		case 'logout': 	$admin->logoutAdmin();
						break;
	}
	$vystup='Vyberte modul pro administraci.';
} else {
	header('Location: ../admin.php?err=notlogin');
}
$admin->assign($vystup, vystup);
$admin->assign($error, error);
$admin->assign('Homepage', heading);
$admin->assign('class="active"', homepage);
$admin->assign($admin->getConfigWeb('title').' - Administrace', title);
?>