<?
/**
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 0.5
 */

require_once ('../include/TText.php');
$text=new TText();
$text->setTemplate('../templates/admin.tpl.html');

//je to admin?
if(isset($_SESSION[admin])){
	switch ($_GET[action]){
		case 'getInsertForm': 	if($_SESSION[admin][accessRight]==1){
									$vystup=$text->getInsertForm('text.php?action=insert');
							  	}
							  	break;
		case 'getEditForm':		$vystup=$text->getEditForm($_GET[id],'text.php?action=update');
								break;

								
		case 'insert':			if($text->insertText($_POST)){
									$text->assign('Text byl vložen', error);
								} else {
									$text->assign('Nepovedlo se vložit', error);
								}
								break;
		case 'update':			$text->editText($_POST);
								break;
		case 'delete':			$text->deleteText($_GET[id]);
								break;
		default: $vystup=$text->getEditTable();
				  break;
	}
} else {
	header('Location: ../admin.php?err=notlogin');
}

$menu='
	<h3>Text</h3>
			<ul class="nav">';
if($_SESSION[admin][accessRight]==1){
	$menu.='<li><a href="text.php?action=getInsertForm">Přidej text</a></li>';
}
$menu.='<li><a href="text.php">Uprav text</a></li>
	</ul>';

$text->assign($vystup, vystup);
$text->assign($table, table);
$text->assign('Texty', heading);
$text->assign($menu, adminmenu);
$text->assign('class="active"', text);
$text->assign($text->getConfigWeb('title').' - Administrace', title);
?>