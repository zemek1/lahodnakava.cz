<?
/**
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 0.5
 */

require_once ('../include/TNews.php');
$news=new TNews();
$news->setTemplate('../templates/admin.tpl.html');

//je to admin?
if(isset($_SESSION[admin])){
	switch ($_GET[action]){
		//zobrazi formular pro pridani news v cz
		case 'form_cz':	$vystup=$news->getInsertForm('news.php?action=insert','cz');
						break;
		//zobrazi tabulku vlozenych news pro cz 
		case 'edit_cz':	$table=$news->getEditTable('cz');
						break;
		//zobrazi formular pro editaci konkretni news
		case 'edit':	$table=$news->getEditForm($_GET[id],'news.php?action=update');
						break;
		//vlozi data z formu do db
		case 'insert': 	if($news->insertRecord($_POST)){
							$err='Úspěšně vloženo';
						} else {
							$err='Nepovedlo se vložit';
						}
		//vymaze news dle id z db
		case 'delete': 	if($news->deleteRecord($_GET[id])){
							$err='Úspěšně smazáno';
						} else {
							$err='Nepovedlo se smazat';
						}
		//upravi news dle dat z formu
		case 'update': 	if($news->editRecord($_POST)){
							$err='Úspěšně upraveno';
						} else {
							$err='Nepovedlo se upravit';
						}
		default: $vystup=$news->getInsertForm('news.php?action=insert','cz');
				  break;
	}
} else {
	header('Location: ../admin.php?err=notlogin');
}

$menu='
	<h3>Novinky</h3>
			<ul class="nav">
				<li><a href="news.php?action=form_cz">Přidej novou</a></li>
				<li><a href="news.php?action=edit_cz">Edituj</a></li>
			</ul>';

$news->assign($vystup, vystup);
$news->assign($table, table);
$news->assign($err, error);
$news->assign('Novinky', heading);
$news->assign($menu, adminmenu);
$news->assign('class="active"', news);
$news->assign($news->getConfigWeb('title').' - Administrace', title);
?>