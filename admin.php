<?
/**
 * 
 * 
 * @author Lukas Zemek
 * @copyright 2009 Lukas Zemek
 * @version 1.0
 */

require_once ('include/TAdmin.php');
$admin=new TAdmin();
$admin->setTemplate('templates/adminlogin.tpl.html');

//error z parametru
if(isset($_GET[err])){
	$error=$admin->getError($_GET[err]);
}

//je admin uz zalogovan?
if($_SESSION[admin]){
	header('Location: admin/index.php');
} else {
	if(empty($_POST)){
		$formular=$admin->getLoginForm('admin.php');
	} else {
		if($admin->isAdmin($_POST[nickName], $_POST[password])){
			$admin->loginAdmin($_POST[nickName], $_POST[password]);
			header('Location: admin/index.php');
		} else {
			$error=$admin->getError('badlogin');
			$formular=$admin->getLoginForm('admin.php');
		}
	}
}
$admin->assign($formular, formular);
$admin->assign($error, error);
?>