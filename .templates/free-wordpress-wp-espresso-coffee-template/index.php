<?php get_header(); ?>

	<div id="left">
<?php if ( $paged < 2 ) { // Do stuff specific to first page?>
<?php $my_query = new WP_Query('showposts=1');
  while ($my_query->have_posts()) : $my_query->the_post();
  $do_not_duplicate = $post->ID;?>

	<div class="post" id="post-<?php the_ID(); ?>">
	<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
	
	<?php the_content('Read more &raquo;'); ?>


	<p class="postmetadata">Posted in <?php the_category(', ') ?> | by <font color="#975956"><?php the_author() ?></font> | <div id="comments-post"><?php comments_popup_link(__('No comments'), __('1 comment'), __('% Comments')); ?></div></p>
			</div>

		<?php endwhile; ?>

<div class="latest"><div class="h2title"><h2>Be Sure To Check Our Latests Posts</h2></div><div class="rss"><a href="<?php bloginfo('rss2_url'); ?>">Entries (RSS)</a></div><div class="clear"></div></div>
<?php if (have_posts()) : while (have_posts()) : the_post(); 
  if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); ?>
<div class="post2" id="post-<?php the_ID(); ?>">
<h2 class="second"><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		<small><?php the_time('F jS, Y') ?> | <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?> </small> 
				<?php the_content_limit(400, "Read More"); ?>
			</div>

<?php endwhile; endif; ?>

<?php } else { // Do stuff specific to non-first page ?>

<?php if (have_posts()) : while (have_posts()) : the_post();
if( $post->ID == $do_not_duplicate ) continue; update_post_caches($posts); ?>
<div class="post" id="post-<?php the_ID(); ?>">
				<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="Permanent Link to <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
				<p class="date"><?php the_time('F jS, Y, l') ?>, <?php the_time() ?> <!-- by <?php the_author() ?> --></p>

							<?php the_content('Read the rest of this entry &raquo;'); ?>
		
				<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> Posted in <?php the_category(', ') ?> | <?php edit_post_link('Edit', '', ' | '); ?>  <?php comments_popup_link('No Comments &#187;', '1 Comment &#187;', '% Comments &#187;'); ?></p>
			</div>
<?php endwhile; endif;  ?>

<?php } ?>

		<div class="navigation">
			<div class="alignleft"><?php next_posts_link('&laquo; Older Entries') ?></div>
			<div class="alignright"><?php previous_posts_link('Newer Entries &raquo;') ?></div>
			<div class="clear"></div>
		</div>




	</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
