<div id="right">

<div id="mytab">
<div class="tabber">
<div class="tabbertab">
<h2>Recent Post</h2>
<ul>
<?php
$myposts = get_posts('numberposts=10&offset=1'); foreach($myposts as $post) : ?>
<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
<?php endforeach; ?>
</ul>
</div>


<div class="tabbertab">
<h2>Recent Comments</h2>
<ul>
<?php get_recent_comments(); ?>
</ul>
</div>


<div class="tabbertab">
<h2>Archives</h2>
<ul>
<?php wp_get_archives('type=monthly'); ?>
</ul>
</div>

<div class="tabbertab">
<h2>Get Newsletter</h2>
<?php include (TEMPLATEPATH . '/feedburner.php'); ?>
</div>

</div><div class="clear"></div></div>

<div id="adsense">
<?php include (TEMPLATEPATH . '/adsense.php'); ?>
<div class="clear"></div></div>

<div id="sidebar">
<div id="lsidebar">
<ul>
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(1) ) : else : ?>
	<?php endif; ?>
	</ul>
</div>
<div id="rsidebar">
<ul>
	<?php if ( function_exists('dynamic_sidebar') && dynamic_sidebar(2) ) : else : ?>
	<?php endif; ?>
	</ul>
</div>
<div class="clear"></div></div>
</div>