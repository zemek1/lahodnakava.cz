<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
<head profile="http://gmpg.org/xfn/11">
<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />
<title><?php bloginfo('name'); ?> <?php if ( is_single() ) { ?> &raquo; Blog Archive <?php } ?> <?php wp_title(); ?></title>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<link rel="stylesheet" href="<?php bloginfo('template_directory'); ?>/tab.css" type="text/css" media="screen" />
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/tabber.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_directory'); ?>/focus.js"></script>
<link rel="alternate" type="application/rss+xml" title="<?php bloginfo('name'); ?> RSS Feed" href="<?php bloginfo('rss2_url'); ?>" />
<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
<?php wp_head(); ?>
</head>
<body>
<div id="box">
<div id="wrapper">

<div id="top">

<form method="get" id="searchform" action="<?php bloginfo('url'); ?>/">
<div id="searchbox">
<input type="text" value="search..." onfocus="if (this.value == 'search...') {this.value = '';}" onblur="if (this.value == '') {this.value = 'search...';}" name="s" id="s" />

<input type="image" src="<?php bloginfo('template_directory'); ?>/images/logo.gif" id="go" alt="Search" title="Search" />
<div class="clear"></div></div>
</form>	

<a href="<?php bloginfo('rss2_url'); ?>" title="RSS"><img src="<?php bloginfo('template_directory'); ?>/images/logo.gif" style="float: right; position: absolute; width: 63px; height: 45px; right: 333px; top: 103px; border:0px;"></a>

<div id="logo"><h1><a href="<?php echo get_option('home'); ?>/" title="Coffee Blog">Coffee Blog</a></h1></div>
</div>



<ul id="navigation">
    <li><a href="<?php bloginfo('url'); ?>" title="Homepage"><span>Homepage</span></a></li>
    <?php wp_list_pages('title_li=&link_before=<span>&link_after=</span>'); ?>
</ul>


<div id="main">